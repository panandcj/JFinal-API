package com.yunfinal.api.tpl.template.directive;

import com.jfinal.template.Directive;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Parser;
import com.jfinal.template.stat.Scope;

/**
 * Enjoy指令扩展eval指令动态化
 * #eval(val)
 * @author  杜福忠
 */
public class EvalDirective extends Directive {
    public void exec(Env env, Scope scope, Writer writer) {
        new Parser(env, new StringBuilder(//
                exprList.eval(scope).toString()),null)//
                .parse().exec(env, scope, writer);
    }
}
