package com.yunfinal.api.tpl.template;

import com.jfinal.template.Engine;
import com.jfinal.template.Template;

import java.util.Map;

/**
 * 模板集中管理配置
 * @author 杜福忠
 */
public class TemplateKit {
    private static Engine me;

    public static void setEngine(Engine me) {
        TemplateKit.me = me;
    }

    /**
     * 获取一个模板对象
     */
    public static Template getTemplate(String fileName){
        return me.getTemplate(fileName);
    }

    /**
     * 根据指定模板，返回渲染后的字符串
     */
    public static String render(String fileName, Map<Object, Object> data){
        return me.getTemplate(fileName).renderToString(data);
    }
}
