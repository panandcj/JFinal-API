package com.yunfinal.api.tpl.json;

import com.jfinal.core.JFinal;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Kv;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.render.Render;
import com.jfinal.template.Template;
import com.jfinal.template.stat.ParseException;
import com.yunfinal.api.tpl.template.TemplateKit;

/**
 * JSON—UI 模版渲染
 *
 * @author 杜福忠 - 2021-04-23
 */
public class ApiController extends ApiBaseController {
    private static final Log log = Log.getLog(ApiController.class);

    /**
     * 渲染SQL文件，数据统一存放在 resources/api/data/*下面，name是相对路径
     */
    public Object data(){
        String name = ApiHandler.getName(this);
        if (StrKit.isBlank(name)){
            return jsonError(400, "参数不正确");
        }
        // 权限
//        String url = "/api/data/".concat(name);
//        if (! AuthKit.isByUrl(getAccount(), url)){
//            return jsonError(401, "该用户没有被赋予【" + url + "】权限，请联系管理员");
//        }
        StringBuilder path = new StringBuilder(128);
        path.append("/api/data/").append(name).append(".sql");
        Template template;
        try {
            template = TemplateKit.getTemplate(path.toString());
        } catch (RuntimeException e) {
            if (e.getMessage().contains("File not found : ")){
                return jsonError(404, "请求路径不正确，请检查后再访问");
            }
            log.error("sql文件语法异常：", e);
            return jsonError(500, "服务器文件异常，帮忙联系管理员处理");
        }
        Kv d;
        if (isContentTypeJson()) {
            d = JsonKit.parse(getRawData(), Kv.class);
        } else {
            d = getKv();
        }
        Kv kv = Kv.create();
        kv.set("this", this);
        kv.set("d", d);
        String str;
        try {
            str = template.renderToString(kv);
            if (JFinal.me().getConstants().getDevMode()){
                //开发模式时，把运行情况打印到控制台
                log.info("执行数据文件日志：\n" + str.replace("--", "").replaceAll("\r\n\n", ""));
            }
        } catch (RuntimeException e) {
            log.error("执行数据文件异常：", e);
            return jsonError(500, "服务器运行异常，帮忙联系管理员处理");
        }
        //默认无对象时处理
        Render render = getRender();
        if(render == null){
            renderText(str);
        }
        return null;
    }

}
