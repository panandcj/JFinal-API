package com.yunfinal.api.tpl.template.directive.db;

import com.jfinal.plugin.activerecord.DbPro;
import com.jfinal.plugin.activerecord.SqlPara;

/**
 * #sqlPara 指令方便定义SqlPara  https://www.jfinal.com/doc/5-13
 * <p>
 * 定义：
 * #findFirst(Record变量名)
 * 在此是SQL模版语法
 * #end
 * <p>
 * @author  杜福忠
 */
public class FindFirstDirective extends SqlParaDirective {

    @Override
    protected Object getData(DbPro db, SqlPara sqlPara) {
        setDefaultName("record");
        return db.findFirst(sqlPara);
    }
}
