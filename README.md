#《JFinal-API 数据模板使用说明书》v2.0

## IDEA 导入语法提示模板
doc/settings.zip   
![导入设置](doc/img/idea/1.png)
![导入设置](doc/img/idea/2.png)


## 语法
[使用JFinal的Enjoy模版引擎的语法](https://jfinal.com/doc/6-1)



## 常用变量

### 前端请求的参数：
`d` 是 JFinal的 Kv 对象拥有它全部的方法
#### 例子:
```
#(d.val)
```
一般查询传递的参数url?val=$(this).val()

```
#if(d.isNull('id')) XXX #end
```
key 不存在，或者 key 存在但 value 为null 的判断

### BaseController对象
`this`
#### 例子:
```
#(this.jsonError()) #return 
错误返回JSON格式{code:-1, msg:"fail"}

#(this.jsonError(400, '缺失name参数')) #return 
错误返回JSON格式{code:400, msg:"缺失name参数"}

#(this.jsonData()) #return 
成功返回JSON格式{code:0, msg:"OK"}

#(this.jsonData(object))
成功返回JSON格式{code:0, msg:"OK", data:Object}

#(this.jsonPage(object))
成功返回JSON格式分页信息{code:0, msg:"OK", data:[Object], count:总行数, pageNumber:页数, pageSize:条数}


```
以上方法如果需要立即结束，需要在方法后面 使用`#return`指令才可结束。

代码逻辑书写规范：
需要把非法业务情况写在前面，成功逻辑写在最后。

### 常用取值工具类
```
查看源代码获取更多用法
StrKit.java 系列方法
TplKit.java 系列方法
DateKit.java 系列方法
TimeKit.java 系列方法
HashKit.java 系列方法
LogKit.java 系列方法
ElKit.java 系列方法
JsonKit.java 系列方法

```

### 全局方法
```
isBlank(String) notBlank(String) 判断是否空字符串
notNull(Object... paras) 判断对象集不为空
now() 返回当前时间 Date对象
eq(a, b) 返回忽略java类型进行比较，如数字 1 和字符串"1" 比较值为相等
notEq(a, b) 返回相反
toJson(obj) 返回转为JSON字符串
contains(x.verify, "required") 返回是否包含该字符串
set(Object obj, String column, Object value)  Record和JSONObject对象赋值后，不输出任何内容 #(set(x, "value", para.id))
TplKit 系列方法,查看源代码获取更多用法，常用：
TplKit.dbSaveOrUpdateById('tableName', d) 根据对象ID值判断是保存还是修改
DateKit.to('2021-12-09 23:02:23') 转换为Date对象
DateKit.to(now()) Date对象转换为字符串

```




## 自定义指令
#### 指令：数据库List查询
```
#para(d.campusid)
```
等价与 Db.find("SQL ? ") 问号挂参, 替换生成为一个问号占位 ”?”
```
select * from article where title like concat('%', #para(title), '%')
```

#### 指令：数据库List查询
```
#find(List变量名)
在此是SQL模版语法
#end
默认返回变量值：list
```
等价与 Db.find(sqlPara)。


#### 指令：数据库Record查询
```
#findFirst(Record变量名)
在此是SQL模版语法
#end
默认返回变量值：record
```
等价与 Db.findFirst(sqlPara)。


#### 指令：数据库分页Page<Record>查询
```
#set(pageNumber=1)
#set(pageSize=15)
#set(isGroupBySql=true)
#paginate(Page<Record>变量名)
在此是SQL模版语法
#end
默认返回变量值：page
```
等价与 Db.paginate(pageNumber, pageSize, isGroupBySql, sqlPara)。

#### 常用写法示例：
```
#paginate()
SELECT * FROM kit_xxx WHERE 1=1
#end
```
一般搭配 `#(this.jsonPage(page))` 使用。

如果要对默认返回的page变量做处理，使用for即可。
```
#for(x : page.getList()) 
    #(set(x, 'mediatorName', AccountKit.getName(x.mediatorid)))
#end
```

#### 指令：数据库修改
```
#update(受影响行数返回结果的变量名)
在此是SQL模版语法
#end
默认返回变量值：result
```
等价与 Db.update(sqlPara)

#### 指令：数据库事务
```
#dbTx()
在此执行的前面数据库指令SQL操作都会开启事务
#end
```
等价与 Db.tx(->{xxx})

### 完整示例：
```
### 数据分页查询
#paginate()
SELECT id, realName stuName, mediatorid FROM account WHERE userType=1 AND STATE=0
    #if(d.campusid)
        AND campusid=#para(d.campusid)
    #end
    #if(d.stuName)
     AND stuName like concat('%', #para(d.stuName), '%')
    #end
    ### 区间日期处理
    #if(d.gmtModified)
        #set(gmtModified = TplKit.splitToDate(d.gmtModified))
        AND a.gmtModified >= #para(gmtModified.start)
        AND a.gmtModified <= #para(gmtModified.end)
    #end
#end
### 二次处理返回集合
#for(x : page.getList())
    ###--注意值转换时，不推荐覆盖系统原有值，需新加一个名词，如：追加xxxName（一般是连表）或xxxStr（一般是值对应汉字）后缀
    #(set(x, 'mediatorName', AccountKit.getName(x.mediatorid)))
    #(set(x, 'campusName', CampusKit.getName(x.campusId))) 
    #(set(x, 'stateStr', x.state == 'on' ? '正常':'关闭'))
#end
### 返回分页数据
#(this.jsonPage(page))
```

## 项目启动：
点击三角图标启动   
![启动项目](doc/img/idea/run-1.png)   
看到笑脸代表启动成功   
![启动项目](doc/img/idea/run-2.png)   
浏览器访问：http://127.0.0.1:8080/api/data/test    
代表执行的是 src/main/resources/api/data/test.sql这个文件   
![启动项目](doc/img/idea/run-3.png)   
